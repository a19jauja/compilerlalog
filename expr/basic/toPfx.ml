open Ast
open BasicPfx.Ast

(* Question 5.2 *)

let rec generate = function
  | Const x -> [Push x]
  | Binop(Badd,a,b) -> (generate b) @ (generate a) @ [Add]
  | Binop(Bsub,a,b) -> (generate b) @ (generate a) @ [Sub]
  | Binop(Bmul,a,b) -> (generate b) @ (generate a) @ [Mul]
  | Binop(Bdiv,a,b) -> (generate b) @ (generate a) @ [Div]
  | Binop(Bmod,a,b) -> (generate b) @ (generate a) @ [Rem]
  | Uminus x -> (generate x) @ [Push 0] @ [Sub]
  | Var _ -> failwith "Not yet supported"
