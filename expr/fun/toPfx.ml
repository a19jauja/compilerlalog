open Ast
open BasicPfx.Ast

let increment env = env := (List.map (fun (name, v) -> print_endline (name ^ " is now associated to index " ^ (string_of_int (v+1))); (name, v+1)) !env)

(* let decrement env = env := (List.map (fun (name, v) -> print_endline (name ^ " is now associated to index " ^ (string_of_int (v-1))); (name, v-1)) !env); [] *)

let add_var var env = env := (var, -1)::(!env) ; print_endline (var ^ " was added to env with index -1")

let rec generate env = function
  | Const x -> print_endline ("Argument is being pushed"); increment env; [Push x]
  | Binop(Badd,a,b) -> let y = (generate env b) in y @ (generate env a) @ [Add]
  | Binop(Bsub,a,b) -> let y = (generate env b) in y @ (generate env a) @ [Sub]
  | Binop(Bmul,a,b) -> let y = (generate env b) in y @ (generate env a) @ [Mul]
  | Binop(Bdiv,a,b) -> let y = (generate env b) in y @ (generate env a) @ [Div]
  | Binop(Bmod,a,b) -> let y = (generate env b) in y @ (generate env a) @ [Rem]
  | App(Fun(v, t), arg) -> (add_var v env); let y = (generate env arg) in y @ [InstructionSeq (generate env t); Exec; Swap; Pop]
  | Uminus x -> (generate env x) @ [Push 0] @ [Sub]
  | Var x -> print_endline ("Argument " ^ x ^ " is being called") ; increment env; [Push ((List.assoc x !env) - 1); Get]
  | _ -> []