%{
  open Ast
%}

(**************
 * The tokens *
 **************)

%token EOF PUSH POP SWAP ADD SUB MUL DIV REM EXEC GET
%token <int> INT


(******************************
 * Entry points of the parser *
 ******************************)

%start <Ast.program> program

%%

(*************
 * The rules *
 *************)

program: 
  | i=INT; c=command { i, c }
  | EOF { 0, [] }

command: 
  | PUSH command { failwith ("Argument missing for push function") }
  | PUSH i=INT c=command { (Push i)::c }
  | POP c=command { Pop::c }
  | SWAP c=command { Swap::c }
  | ADD c=command { Add::c }
  | SUB c=command { Sub::c }
  | MUL c=command { Mul::c }
  | DIV c=command { Div::c }
  | REM c=command { Rem::c }
  | EXEC c=command { Exec::c }
  | GET c=command { Get::c }
  | EOF { [] }

%%
