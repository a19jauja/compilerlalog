(* Question 6.1 et 6.2 *)
{
  open Parser
  (* open Ast *)
  open Utils

  let mk_int nb lexbuf =
    try INT (int_of_string nb)
    with Failure _ -> failwith (Location.string_of (Location.curr lexbuf))
}

let newline = (['\n' '\r'] | "\r\n")
let blank = [' ' '\014' '\t' '\012']
let not_newline_char = [^ '\n' '\r']
let digit = ['0'-'9']

rule token = parse
  (* newlines *)
  | newline { token lexbuf }
  (* blanks *)
  | blank + { token lexbuf }
  (* end of file *)
  | eof      { EOF }
  (* comments *)
  | "--" not_newline_char*  { token lexbuf }
  (* integers *)
  | digit+ as nb           { mk_int nb lexbuf }
  (* commands  *)
  | "push" {PUSH}
  | "pop" {POP}
  | "swap" {SWAP}
  | "add" {ADD}
  | "sub" {SUB}
  | "mul" {MUL}
  | "div" {DIV}
  | "rem" {REM}
  | "exec" {EXEC}
  | "get" {GET}
  (* illegal characters *)
  | _                  { failwith (Location.string_of (Location.curr lexbuf)) }